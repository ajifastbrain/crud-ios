//
//  IntegerExtension.swift
//  CRUD
//
//  Created by Aji Prakosa on 02/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

extension Int {
    func convertToCurrency(withRp: Bool = false) -> String{
        let formatter                   = NumberFormatter()
               formatter.locale                = Locale.current //Locale(identifier: "es_ES")
               formatter.numberStyle           = .currency
               formatter.currencySymbol        = ""
               formatter.maximumFractionDigits = 0
        if withRp {
            let rp = "Rp\((formatter.string(from: self as NSNumber) ?? "").replacingOccurrences(of: ".", with: ","))"
            if rp.contains("-") {
              return "-\(rp.replacingOccurrences(of: "-", with: ""))"
            } else {
              return rp
          }
        } else {
            return (formatter.string(from: self as NSNumber) ?? "").replacingOccurrences(of: ".", with: ",")
        }
    }
    
    static func parse(from string: String) -> Int? {
        let decimals = string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        return Int(decimals)
    }
}
