//
//  Strings.swift
//  CRUD
//
//  Created by Aji Prakosa on 02/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

struct Localization {
    struct Label {
        static let SOMETHING_WENT_WRONG = "Something Went Wrong".localized
        static let CREATE_EMPLOYEE = "Create Employee".localized
        static let UPDATE_EMPLOYEE = "Update Employee".localized
        static let FAILED = "Failed".localized
        static let SUCCESS_CREATE_EMPLOYEE = "Successfully Creating Employee Data".localized
        static let NAME = "Name:".localized
        static let SALARY = "Salary:".localized
        static let AGE = "Age:".localized
        static let EMPLOYEE_LIST = "Employee List".localized
        static let SHOW_EMPLOYEE = "Show All Employees".localized
        static let EDIT = "Edit".localized
        static let DELETE = "Delete".localized
        static let WARNING = "Warning".localized
        static let DELETE_DATA = "Are you sure you want to delete data?".localized
        static let OKAY = "OK".localized
        static let CANCEL = "Cancel".localized
    }
}
