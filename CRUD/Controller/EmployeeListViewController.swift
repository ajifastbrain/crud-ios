//
//  EmployeeListViewController.swift
//  CRUD
//
//  Created by Macintosh on 02/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class EmployeeListViewController: BaseViewController {
    
    @IBOutlet weak var mainTable: UITableView!
    
    private var numRowx = Int()
    
    private var employeeListModels: EmployeeList?
    
    override func viewWillAppear(_ animated: Bool) {
        self.getAPI()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBarTitle(withTitle: Localization.Label.EMPLOYEE_LIST)
        
        mainTable.register(UINib(nibName: "ListItemCell", bundle: nil), forCellReuseIdentifier: "ListItemCell")
        
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.separatorStyle = .none
    }
    
    func getAPI() {
        self.showLoadingView()
        API.getEmployeeList(completion: { (dataEmployeeList, error) in
             if let dataModel = dataEmployeeList {
                 self.employeeListModels = dataModel
                 self.numRowx = self.employeeListModels?.data?.count ?? 0
                 self.mainTable.reloadData()
                 self.hideLoadingView()
             } else if let error = error {
                self.hideLoadingView()
                self.showAlertWith(title: "Error", message: error, action: nil)
             }
        })
    }
    
    @objc func onEdit(sender: UIButton) {
        let id: String = self.employeeListModels?.data?[sender.tag].id ?? ""
        let name: String = self.employeeListModels?.data?[sender.tag].employee_name ?? ""
        let salary: String = self.employeeListModels?.data?[sender.tag].employee_salary ?? ""
        let age: String = self.employeeListModels?.data?[sender.tag].employee_age ?? ""
        self.navigationController?.pushViewController(EditingViewController(id: id, name: name, salary: salary, age: age), animated: true)
    }
    
    @objc func onDelete(sender: UIButton) {
        let id: String = self.employeeListModels?.data?[sender.tag].id ?? ""
        let refreshAlert = UIAlertController(title: Localization.Label.WARNING, message: Localization.Label.DELETE_DATA, preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: Localization.Label.OKAY, style: .default, handler: { (action: UIAlertAction!) in
              self.showLoadingView()
              API.deleteEmployee(id: id, completion: { [weak self] (error) in
                  guard let strongSelf = self else {
                      self?.hideLoadingView()
                      return
                  }
                  strongSelf.hideLoadingView()
                  
                  if let error = error {
                      self?.hideLoadingView()
                      strongSelf.showAlertWith(title: Localization.Label.FAILED, message: error, action: {
                          strongSelf.dismiss(animated: true, completion: nil)
                      })
                  } else {
                      self?.hideLoadingView()
                      self?.getAPI()
                  }
              })

        }))

        refreshAlert.addAction(UIAlertAction(title: Localization.Label.CANCEL, style: .cancel, handler: { (action: UIAlertAction!) in
              print("Handle Cancel Logic here")
        }))

        present(refreshAlert, animated: true, completion: nil)
    }
}


extension EmployeeListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numRowx
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        cell = mainTable.dequeueReusableCell(withIdentifier: "ListItemCell", for: indexPath as IndexPath) as! ListItemCell
        let cells = cell as! ListItemCell
        let id: String = self.employeeListModels?.data?[indexPath.row].id ?? ""
        let name: String = self.employeeListModels?.data?[indexPath.row].employee_name ?? ""
        let salary: String = self.employeeListModels?.data?[indexPath.row].employee_salary ?? ""
        let age: String = self.employeeListModels?.data?[indexPath.row].employee_age ?? ""
        cells.configureView(id: id, name: name, salary: salary, age: age)
        cells.btnEdit.tag = indexPath.row
        cells.btnDelete.tag = indexPath.row
        cells.btnEdit.addTarget(self, action: #selector(onEdit(sender:)), for: .touchUpInside)
        cells.btnDelete.addTarget(self, action: #selector(onDelete(sender:)), for: .touchUpInside)
        cells.selectionStyle = .none
        
        if (indexPath.row % 2 == 0) {
            cells.layer.backgroundColor = UIColor.init(hexString: "#eeeeee").cgColor
        } else {
            cells.layer.backgroundColor = UIColor.init(hexString: "#c4c9bf").cgColor
        }
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 211
    }
}
