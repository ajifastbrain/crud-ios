//
//  EditingViewController.swift
//  CRUD
//
//  Created by Macintosh on 02/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class EditingViewController: BaseViewController {

    @IBOutlet weak var txtName: JVFloatLabeledTextField!
    
    @IBOutlet weak var txtSalary: JVFloatLabeledTextField!
    
    @IBOutlet weak var txtAge: JVFloatLabeledTextField!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    private var id = String()
    
    private var name = String()
    
    private var salary = String()
    
    private var age = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Localization.Label.UPDATE_EMPLOYEE
        txtName.setLeftPaddingPoints(15)
        txtName.setRightPaddingPoints(15)
        
        txtSalary.setLeftPaddingPoints(15)
        txtSalary.setRightPaddingPoints(15)
        
        txtAge.setLeftPaddingPoints(15)
        txtAge.setRightPaddingPoints(15)
        
        btnSubmit.layer.cornerRadius = 5
        
        self.txtName.text = name
        self.txtSalary.text = self.salary
        self.txtSalary.text = salary
        self.txtAge.text = age
    }

    convenience init(id: String, name: String, salary: String, age: String) {
        self.init()
        self.id = id
        self.name = name
        self.salary = salary
        self.age = age
    }

    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        self.showLoadingView()
        API.updateEmployee(id: self.id, name: self.txtName.text ?? "", salary: self.txtSalary.text ?? "", age: self.txtAge.text ?? "", completion: { [weak self] (error) in
            guard let strongSelf = self else {
                self?.hideLoadingView()
                return
            }
            strongSelf.hideLoadingView()
            
            if let error = error {
                self?.hideLoadingView()
                strongSelf.showAlertWith(title: Localization.Label.FAILED, message: error, action: {
                    strongSelf.dismiss(animated: true, completion: nil)
                })
            } else {
                self?.hideLoadingView()
                self?.navigationController?.popViewController(animated: true)
            }
        })
    }
    
}
