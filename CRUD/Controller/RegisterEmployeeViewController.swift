//
//  RegisterEmployeeViewController.swift
//  CRUD
//
//  Created by Macintosh on 02/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class RegisterEmployeeViewController: BaseViewController {
    @IBOutlet weak var txtName: JVFloatLabeledTextField!
    
    @IBOutlet weak var txtSallary: JVFloatLabeledTextField!
    
    @IBOutlet weak var txtAge: JVFloatLabeledTextField!
    
    private var createEmployeeParam: RequestCreateEmployee?
    
    private var resultCreateEmployee: ResultCreateEmployee?
    
    @IBOutlet weak var btnShowListEmployee: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Localization.Label.CREATE_EMPLOYEE
        txtName.setLeftPaddingPoints(15)
        txtName.setRightPaddingPoints(15)
        
        txtSallary.setLeftPaddingPoints(15)
        txtSallary.setRightPaddingPoints(15)
        
        txtAge.setLeftPaddingPoints(15)
        txtAge.setRightPaddingPoints(15)
        
        btnShowListEmployee.setTitle(Localization.Label.SHOW_EMPLOYEE, for: .normal)
    }

    @IBAction func textFieldDidChange(_ sender: UITextField) {
        guard let sallary = Int.parse(from: sender.text!) else { return }
        sender.text = sallary.convertToCurrency()
        
    }
    
    @IBAction func SubmitTapped(_ sender: UIButton) {
        self.showLoadingView()
        API.createEmployee(name: self.txtName.text ?? "", salary: self.txtSallary.text ?? "", age: self.txtAge.text ?? "", completion: { [weak self] (error) in
            guard let strongSelf = self else {
                self?.hideLoadingView()
                return
            }
            strongSelf.hideLoadingView()
            
            if let error = error {
                strongSelf.showAlertWith(title: Localization.Label.FAILED, message: error, action: {
                    strongSelf.dismiss(animated: true, completion: nil)
                })
            } else {
                strongSelf.showAlertWith(title: Localization.Label.SUCCESS_CREATE_EMPLOYEE, message: "", action: {
                    strongSelf.dismiss(animated: true, completion: nil)
                })
            }
        })
    }
    
    @IBAction func showAllEmployeeTapped(_ sender: UIButton) {
        self.navigationController?.pushViewController(EmployeeListViewController(), animated: true)
    }
    
}
