//
//  AppDelegate.swift
//  CRUD
//
//  Created by Macintosh on 02/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let rootVC = RegisterEmployeeViewController()
        
        let navController = BaseNavigationController(rootViewController: rootVC)
        window?.rootViewController = navController
        
        IQKeyboardManager.shared.enable = true
        
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        
        return true
    }

    
}

