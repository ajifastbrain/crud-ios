//
//  ResultCreateEmployee.swift
//  CRUD
//
//  Created by Aji Prakosa on 02/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import ObjectMapper

class ResultCreateEmployee: Mappable {
    var status: String?
    var data: DataModel?
    
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        data <- map["data"]
    }
}

class DataModel: Mappable {
    var name: String?
    var salary: String?
    var age: String?
    var id: Int?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        salary <- map["salary"]
        age <- map["age"]
        id <- map["id"]
    }
}
