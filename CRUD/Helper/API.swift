//
//  API.swift
//  CRUD
//
//  Created by Aji Prakosa on 02/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class API: NSObject {
    class func resultProcessor(response: (DataResponse<Any>), completion: (_ data: AnyObject?, _ error: String?) -> Void) {
        var apiError: String?
        var body: AnyObject?
        
        switch response.result {
        case .failure(let error):
            Logger.log("Error coy: \(error)")
            apiError = error.localizedDescription
        case .success(let json):
            Logger.log(json)
            if let httpResponse: HTTPURLResponse = response.response, httpResponse.statusCode == 200 || httpResponse.statusCode == 201 || httpResponse.statusCode == 204 {
                body = json as AnyObject?
            } else {
                if let JSON = json as? [ String : AnyObject ] {
                    if let errorDesc = JSON["error_description"] {
                        apiError = errorDesc as? String
                    } else if let errorMessage = JSON["message"] {
                        if errorMessage is [ String : AnyObject ] {
                            if let message = errorMessage["error"] as? String {
                                apiError = message
                            } else if let message = errorMessage["error_message"] as? String {
                                apiError = message
                            }
                        } else {
                            apiError = errorMessage as? String
                        }
                    } else if let error = JSON["error"] as? String {
                        apiError = error
                    } else {
                        apiError = Localization.Label.SOMETHING_WENT_WRONG
                    }
                } else {
                    apiError = Localization.Label.SOMETHING_WENT_WRONG
                }
            }
        }
        
        completion(body, apiError)
    }
    
    class func updateEmployee(id: String, name: String, salary: String, age: String, completion: @escaping (_ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.updateEmployee + id
        let param: [String : String] = [
            "name" : name,
            "salary" : salary,
            "age" : age
        ]
        Logger.log(url)
        Logger.log(param as Any)
        
        NetworkManager.request(.put, url, parameters: param as [String : AnyObject], encoding: JSONEncoding.default, useBasicToken: false, logoutIf401: true, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                completion(error)
            })
        })
    }
    
    class func deleteEmployee(id: String, completion: @escaping (_ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.deleteEmployee + id
        Logger.log(url)
        NetworkManager.request(.delete, url, parameters: nil, encoding: JSONEncoding.default, useBasicToken: false, logoutIf401: true, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                completion(error)
            })
        })
    }

    class func createEmployee(name: String, salary: String, age: String, completion: @escaping (_ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.createEmployee
        let param: [String : String] = [
            "name" : name,
            "salary" : salary,
            "age" : age
        ]
        Logger.log(url)
        Logger.log(param as Any)
        
        NetworkManager.request(.post, url, parameters: param as [String : AnyObject], encoding: JSONEncoding.default, useBasicToken: false, logoutIf401: true, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                completion(error)
            })
        })
    }
    

    class func getEmployeeList(completion: @escaping (_ dataEmployeeList: EmployeeList?, _ error: String?) -> Void) {
        let url = Configuration.baseURL() + APIEndPoint.employeeList
        Logger.log(url)
        
        NetworkManager.request(.get, url, encoding: JSONEncoding.default, completion: { (response) in
            API.resultProcessor(response: response, completion: { (json, error) in
                var dataEmployeeList: EmployeeList?
                var apiError: String?
                if let json = json {
                    dataEmployeeList = Mapper<EmployeeList>().map(JSONString: stringify(json: json))
                } else if let err = error {
                    apiError = err
                }
                completion(dataEmployeeList, apiError)
            })
        })
    }
    
    static func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
          options = JSONSerialization.WritingOptions.prettyPrinted
        }

        do {
          let data = try JSONSerialization.data(withJSONObject: json, options: options)
          if let string = String(data: data, encoding: String.Encoding.utf8) {
            return string
          }
        } catch {
          print(error)
        }

        return ""
    }

}

