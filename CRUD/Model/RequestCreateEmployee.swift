//
//  RequestCreateEmployee.swift
//  CRUD
//
//  Created by Aji Prakosa on 02/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//
import ObjectMapper

class RequestCreateEmployee: Mappable {
    var name: String?
    var salary: String?
    var age: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        salary <- map["salary"]
        age <- map["age"]
    }
}
