//
//  EmployeeList.swift
//  CRUD
//
//  Created by Aji Prakosa on 02/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import ObjectMapper

class EmployeeList: Mappable {
    var status: String?
    var data: [DataListEmployee]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        data <- map["data"]
    }
}


class DataListEmployee: Mappable {
    var id: String?
    var employee_name: String?
    var employee_salary: String?
    var employee_age: String?
    var profile_image: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        employee_name <- map["employee_name"]
        employee_salary <- map["employee_salary"]
        employee_age <- map["employee_age"]
        profile_image <- map["profile_image"]
    }
}
