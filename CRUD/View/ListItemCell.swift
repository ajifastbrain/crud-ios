//
//  ListItemCell.swift
//  CRUD
//
//  Created by Macintosh on 02/07/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class ListItemCell: UITableViewCell {

    @IBOutlet weak var labelID: UILabel!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelSallary: UILabel!
    
    @IBOutlet weak var labelAge: UILabel!
    
    @IBOutlet weak var labelValID: UILabel!
    
    @IBOutlet weak var labelValName: UILabel!
    
    @IBOutlet weak var labelValSallary: UILabel!
    
    @IBOutlet weak var labelValAge: UILabel!
    
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var btnDelete: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelID.font = UIFont.appFont(fontType: FontType.semiBold, fontSize: FontSize.M)
        labelName.font = UIFont.appFont(fontType: FontType.semiBold, fontSize: FontSize.M)
        labelSallary.font = UIFont.appFont(fontType: FontType.semiBold, fontSize: FontSize.M)
        labelAge.font = UIFont.appFont(fontType: FontType.semiBold, fontSize: FontSize.M)
        
        labelValID.font = UIFont.appFont(fontType: FontType.regular, fontSize: FontSize.M)
        labelValName.font = UIFont.appFont(fontType: FontType.regular, fontSize: FontSize.M)
        labelValSallary.font = UIFont.appFont(fontType: FontType.regular, fontSize: FontSize.M)
        labelValAge.font = UIFont.appFont(fontType: FontType.regular, fontSize: FontSize.M)
        
        
        labelID.text = "ID:"
        labelName.text = Localization.Label.NAME
        labelSallary.text = Localization.Label.SALARY
        labelAge.text = Localization.Label.AGE
        btnEdit.setTitle(Localization.Label.EDIT, for: .normal)
        btnDelete.setTitle(Localization.Label.DELETE, for: .normal)
        
        btnEdit.layer.cornerRadius = 5
        btnDelete.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView(id: String, name: String, salary: String, age: String) {
        self.labelValID.text = id
        self.labelValName.text = name
        self.labelValSallary.text = salary
        self.labelValAge.text = age
    }
    
}
